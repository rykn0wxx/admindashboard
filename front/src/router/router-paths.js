export default [
  {
    path: '/',
    name: 'Home',
    view: 'Home',
    icon: 'home'
  }, {
    path: '/about',
    name: 'About',
    view: 'About',
    icon: 'info'
  }
]
